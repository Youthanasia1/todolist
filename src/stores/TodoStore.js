
import { makeAutoObservable, runInAction } from "mobx";
import { getTodos, addTodo, updateTodo, deleteTodo } from "../api";
import { spy } from "mobx" ;



spy((event) => {
    if (event.type === 'action') {
        console.log(event);
    }

})

class TodoStore {
    todos = [];

    constructor() {
        makeAutoObservable(this);
        this.loadTodos();
    }

    loadTodos = async () => {
        try {
            const todos = await getTodos();
            runInAction(() => {
                this.todos = todos;
            });
        } catch (error) {
            console.error("Failed to load todos:", error);
        }
    }

    addTodo = async (title) => {
        try {
            const newTodo = await addTodo(title);
            runInAction(() => {
                this.todos.push(newTodo);
                
            });
        } catch (error) {
            console.error("Failed to add todo:", error);
        }
    }

    

    removeTodo = async (id) => {
        try {
            await deleteTodo(id);
            runInAction(() => {
                this.todos = this.todos.filter(todo => todo._id !== id);
            });
        } catch (error) {
            console.error("Failed to remove todo:", error);
        }
    }

    toggleTodo = async (id) => {
        const todo = this.todos.find(todo => todo._id === id);
        if (todo) {
            try {
                const updatedTodo = await updateTodo(id, { completed: !todo.completed });
                runInAction(() => {
                    todo.completed = updatedTodo.completed;
                });
            } catch (error) {
                console.error("Failed to toggle todo:", error);
            }
        }
    }

    editTodo = async (id, title) => {
        try {
            const updatedTodo = await updateTodo(id, { title });
            runInAction(() => {
                const todo = this.todos.find(todo => todo._id === id);
                if (todo) {
                 
                    todo.title = updatedTodo.title;
                }
            });
        } catch (error) {
            console.error("Failed to edit todo:", error);
        }
    }
}

export default new TodoStore();
