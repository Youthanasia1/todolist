
import React from "react";
import TodoList from "./components/TodoList";
import Basic from "./components/basik"
import './App.css'; 

function App() {
    return (
        <div className="App_main">
            
            <TodoList />
            
        </div>
    );
}

export default App;
