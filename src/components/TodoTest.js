import React, { useState } from "react";
import { observer } from "mobx-react-lite";
import todoStore from "../stores/TodoStore";
import TodoItem from "./TodoItem";
import './TodoList.css'; 
import { Formik, Field, Form } from 'formik';


const TodoList = observer(() => {
    const [newTodo, setNewTodo] = useState("");

    const handleSubmit = (e) => {
        e.preventDefault();
        if (newTodo.trim()) {
            todoStore.addTodo(newTodo);
            setNewTodo("");
        }
    };

    return (
        <div className="todo-list">
            <form onSubmit={handleSubmit} className="todo-form">
                <input
                    type="text"
                    value={newTodo}
                    onChange={(e) => setNewTodo(e.target.value)}
                    placeholder="Добавить задачу"
                    className="todo-input"
                />
                <button type="submit" className="todo-add-button">Добавить</button>
            </form>
            <table className="todo-table">
                <thead>
                    <tr>
                        <th>Выполнение</th>
                        <th>Задача</th>
                        <th>Изменить</th>
                        <th>Удалить</th>
                    </tr>
                </thead>
                <tbody>
                    {todoStore.todos.map(todo => (
                        <TodoItem key={todo._id} todo={todo} />
                    ))}
                </tbody>
            </table>
        </div>
    );
});

export default TodoList;
