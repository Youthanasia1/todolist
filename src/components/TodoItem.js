import React, { useState } from "react";
import { observer } from "mobx-react-lite";
import todoStore from "../stores/TodoStore";
import { Formik, Field, Form } from 'formik';
import * as Yup from 'yup';

const TodoItem = observer(({ todo }) => {
    const [isEditing, setIsEditing] = useState(false);
    const [newTitle, setNewTitle] = useState(todo.title);

    const handleEditSubmit = (values) => {
        todoStore.editTodo(todo._id, values.newTitle);
        setIsEditing(false);
    };

    return (
        <tr className="todo-item">
            <td>
                <Formik
                    initialValues = {{
                        completed: todo.completed,
                    }}
                    onSubmit={() => {}}
                   
                >
                    {({ values, setFieldValue}) => (
                    <Form className ="todo-checkbox">
                        <label>
                            <Field type="checkbox" name="todo-checkbox" checked={values.completed} 
                            onChange={(e) => {
                                setFieldValue("completed", e.target.checked);
                                todoStore.toggleTodo(todo._id);
                            }}
                            />
                        </label>
                    
                    </Form>
                    )}
                </Formik>

                {/*
                <input
                    type="checkbox"
                    checked={todo.completed}
                    onChange={() => todoStore.toggleTodo(todo._id)}
                    className="todo-checkbox"
                />
                */}
                
            </td>
            <td>
                <Formik
                    initialValues={{ newTitle: todo.title }}
                    onSubmit={handleEditSubmit}
                >
                    {({ values, handleChange, handleSubmit }) => (
                        <Form onSubmit={handleSubmit}>
                            {isEditing ? (
                                <Field
                                    as="textarea"
                                    name="newTitle"
                                    value={values.newTitle}
                                    onChange={handleChange}
                                    className="todo-textarea"
                                />
                            ) : (
                                <span
                                    className="todo-title"
                                    style={{ textDecoration: todo.completed ? "line-through" : "none" }}
                                >
                                    {todo.title}
                                </span>
                            )}
                            {isEditing && (
                                <button type="submit" className="todo-edit-button">
                                    Сохранить
                                </button>
                            )}
                        </Form>
                    )}
                </Formik>
            </td>

            <td>
                {isEditing ? (
                    <button
                        onClick={() => setIsEditing(false)}
                        className="todo-edit-button"
                    >
                        Отмена
                    </button>
                ) : (
                    <button
                        onClick={() => setIsEditing(true)}
                        className="todo-edit-button"
                    >
                        Изменить
                    </button>
                )}
            </td>

            <td>
                <button onClick={() => todoStore.removeTodo(todo._id)} className="todo-delete-button">Удалить</button>
            </td>
        </tr>
    );
});

export default TodoItem;
