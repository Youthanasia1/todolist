import React from "react";
import { observer } from "mobx-react-lite";
import todoStore from "../stores/TodoStore";
import TodoItem from "./TodoItem";
import './TodoList.css'; 
import { Formik, Field, Form } from 'formik';
import * as Yup from 'yup';

const TodoList = observer(() => {
    return (
        
        <div className="todo-list">
            <h1>
                Список задач
            </h1>
            <Formik
                initialValues={{ newTodo: '' }}
                validationSchema={Yup.object({
                    newTodo: Yup.string().min(4, 'Слишком мало').max(30, 'Слишком много').required('Введите задачу'),
                })}
                onSubmit={(values, { setSubmitting, resetForm }) => {
                    if (values.newTodo.trim()) {
                        todoStore.addTodo(values.newTodo);
                        resetForm();
                    }
                    setSubmitting(false);
                }}
            >
                {({ isSubmitting, errors, touched }) => (
                    <Form className="todo-form">
                        <div className="input-group">
                            <Field
                                type="text"
                                name="newTodo"
                                placeholder="Добавить задачу"
                                className={`todo-input ${errors.newTodo && touched.newTodo ? 'input-error' : ''}`}
                            />
                            <button type="submit" className="todo-add-button" disabled={isSubmitting}>
                                Добавить
                            </button>
                        </div>
                        {errors.newTodo && touched.newTodo ? (
                            <div className="error-message">{errors.newTodo}</div>
                        ) : null}
                    </Form>
                )}
            </Formik>
            <table className="todo-table">
                <thead>
                    <tr>
                        <th>Выполнение</th>
                        <th>Задача</th>
                        <th>Изменить</th>
                        <th>Удалить</th>
                    </tr>
                </thead>
                <tbody>
                    {todoStore.todos.map(todo => (
                        <TodoItem key={todo._id} todo={todo} />
                    ))}
                </tbody>
            </table>
        </div>
    );
});

export default TodoList;
