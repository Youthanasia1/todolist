
const express = require('express');
const router = express.Router();
const Todo = require('../models/Todo');


router.get('/', async (req, res) => {
    try {
        const todos = await Todo.find();
        res.json(todos);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});


router.post('/', async (req, res) => {
    const todo = new Todo({
        title: req.body.title,
    });
    try {
        const newTodo = await todo.save();
        res.status(201).json(newTodo);
    } catch (err) {
        res.status(400).json({ message: err.message });
    }
});

router.put('/:id', async (req, res) => {
    console.log(`Updating todo with id ${req.params.id}`);
    try {
        const updatedTodo = await Todo.findByIdAndUpdate(
            req.params.id,
            { title: req.body.title, completed: req.body.completed },
            { new: true, runValidators: true }
        );
        if (!updatedTodo) {
            return res.status(404).json({ message: 'Todo not found' });
        }
        res.json(updatedTodo);
    } catch (err) {
        console.error(`Error updating todo with id ${req.params.id}:`, err.message);
        res.status(400).json({ message: err.message });
    }
});

router.delete('/:id', async (req, res) => {
    console.log(`Deleting todo with id ${req.params.id}`);
    try {
        const todo = await Todo.findByIdAndDelete(req.params.id);
        if (!todo) {
            return res.status(404).json({ message: 'Todo not found' });
        }
        res.json({ message: 'Todo deleted' });
    } catch (err) {
        console.error(`Error deleting todo with id ${req.params.id}:`, err.message);
        res.status(500).json({ message: err.message });
    }
});

module.exports = router;
